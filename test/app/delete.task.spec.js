const { expect } = require('chai')
const request = require('supertest')
const { manageInMemoryDatabase } = require('../index.js')
const { createApp } = require('../../src/app.js')

describe('Testing delete task via REST', () => {
  manageInMemoryDatabase()
  let app
  beforeEach(() => {
    const amqpChannel = { consume: () => {}, sendToQueue: () => {} }
    app = createApp({ amqpChannel })
  })

  it('When delete task created should return 200', () => {
    return request(app).post('/tasks/')
      .then(response => {
        expect(response.status).to.be.equal(201)
        return response.body.id
      })
      .then(id => request(app).get(`/tasks/${id}`))
      .then(response => {
        expect(response.status).to.be.equal(200)
        return response.body.id
      })
      .then(id => request(app).delete(`/tasks/${id}`))
      .then(response => {
        return request(app).get(`/tasks/${response.body.id}`)
      })
      .then(response => {
        expect(response.status).to.be.equal(404)
        return response.body.id
      })
  })
})
