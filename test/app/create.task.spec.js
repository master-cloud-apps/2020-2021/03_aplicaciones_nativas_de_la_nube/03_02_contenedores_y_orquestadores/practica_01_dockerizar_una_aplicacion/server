const { expect } = require('chai')
const request = require('supertest')
const { manageInMemoryDatabase } = require('../index.js')
const { createApp } = require('./../../src/app.js')

describe('Testing create task via REST', () => {
  manageInMemoryDatabase()
  let app
  beforeEach(() => {
    const amqpChannel = { consume: () => {}, sendToQueue: () => {} }
    app = createApp({ amqpChannel })
  })

  it('When create task should return 201', () => {
    return request(app).post('/tasks/')
      .then(response => expect(response.status).to.be.equal(201))
  })
  it('When create task should return 201', () => {
    return request(app).post('/tasks/')
      .then(response => expect(response.status).to.be.equal(201))
  })
})
