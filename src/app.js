const express = require('express')
const expressWs = require('express-ws')
const { processTask, consumeTask } = require('./queue.js')
const { createWebsocket } = require('./websocket.js')
const Task = require('./models/task.js')
const random = require('random-integer')

const createApp = ({ amqpChannel }) => {
  const app = express()

  app.use(express.static('public'))
  app.use(express.json())
  expressWs(app)

  app.route('/tasks/:id').get((req, res) => {
    return Task.find({ id: req.params.id })
      .then(tasks => {
        if (tasks === undefined || tasks.length === 0) {
          console.log(`No task with id ${req.params.id}`)
          return res.status(404).end()
        }
        return res.send(tasks[0])
      })
  })

  app.route('/tasks/:id').delete((req, res) => {
    return Task.find({ id: req.params.id })
      .then(tasks => {
        return Task.findOneAndDelete({ _id: tasks[0]._id })
      })
      .then(task => {
        return res.status(200).send(task)
      })
  })

  app.post('/tasks/', (req, res) => {
    const task = {
      id: random(1024),
      text: req.body.text,
      progress: 0,
      completed: false
    }

    return new Task(task).save().then(newTask => {
      res.status(201).send(newTask)
      return processTask({ amqpChannel, task: newTask })
    })
  })

  createWebsocket({ app })
  consumeTask({ amqpChannel })

  return app
}

module.exports = {
  createApp
}
