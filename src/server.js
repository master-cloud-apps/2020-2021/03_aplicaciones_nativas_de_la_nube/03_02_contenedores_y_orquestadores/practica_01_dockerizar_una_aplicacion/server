const amqp = require('amqplib/callback_api')
const { createApp } = require('./app')
const { connect } = require('./db/mongoose.js')

let amqpChannel
const AMQP_URL = process.env.AMQP_URL || 'amqp://guest:guest@localhost'
const MONGO_URL = process.env.MONGO_URL || 'mongodb://localhost:27017/tasksDB'

console.log(`Conf uris: ${AMQP_URL}, ${MONGO_URL}`)

amqp.connect(AMQP_URL, async function (err, conn) {
  if (err !== undefined && err !== null) {
    console.log('Error connecting to Rabbit')
    console.log(err)
    process.exit(1)
  }

  amqpChannel = await conn.createChannel()

  // queues creation
  await amqpChannel.assertQueue('tasksProgress')
  await amqpChannel.assertQueue('newTasks')

  console.log(`Connected to RabbitMQ: ${AMQP_URL}`)

  connect(MONGO_URL).then(() => {
    console.log(`Connected to ${MONGO_URL}`)
    const app = createApp({ amqpChannel })
    app.listen(8080, () => console.log('listening on 8080'))
  })
})
