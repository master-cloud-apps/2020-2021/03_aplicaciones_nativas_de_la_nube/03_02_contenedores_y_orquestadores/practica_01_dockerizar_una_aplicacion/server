let userWs

const createWebsocket = ({ app }) => {
  app.ws('/taskProgress', function (ws, req) {
    console.log('User connected')
    userWs = ws
  })
}

const sendMessageToSavedWebSocket = (message) => {
  if (userWs) {
    userWs.send(JSON.stringify(message))
  }
}

module.exports = {
  createWebsocket,
  sendMessageToSavedWebSocket
}
